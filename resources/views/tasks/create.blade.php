@extends('layout')



@section('content')
<div class="container">
    <h3>Create task</h3>

    <div class="row">
        <div class="col-md-10">
            {!! Form::open(['route' => ['tasks.store']]) !!}

            <div class="form-group">
                <input type="text" class="from-control" name="title" value="{{ old('title') }}">
                <br>
                <textarea name="description" id="" cols="30" rows="10" class="from-control">{{ old('description') }}</textarea>
                <br>
                <button class="btn btn-success">Submit</button>
            </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection